﻿using System;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class Item : MonoBehaviour
    {
        public Transform body;
        public float zRotationSpeed = 60.0f;
        public float floatFrequency = 1.0f / 6.0f;
        public float floatAmplitude = 0.5f;
        public float zShift = 0.0f;
        public AudioClip pickUpAudioClip;
        public AudioClip thudAudioClip;
        
        private BoxCollider _boxCollider;
        private Rigidbody _rigidbody;
        private float _zRotationOffset = 0.0f;
        private float _floatCycleOffset = 0.0f;
        
        private void Awake()
        {
            _boxCollider = GetComponent<BoxCollider>();
            if (_boxCollider == null)
                throw new MissingComponentException("BoxCollider is needed for Item");
            _rigidbody = GetComponent<Rigidbody>();
            if (_rigidbody == null)
                throw new MissingComponentException("Rigidbody is needed for Item");
            if (body == null)
                throw new InvalidOperationException("Item.childObject is not set");
            if (pickUpAudioClip == null)
                throw new InvalidOperationException("Item.pickUpAudioClip is not set");
            if (thudAudioClip == null)
                throw new InvalidOperationException("Item.thudAudioClip is not set");
            _zRotationOffset = UnityEngine.Random.value * 360.0f;
            _floatCycleOffset = UnityEngine.Random.value;
        }

        private void Update()
        {
            if (body == null)
                return;
            body.localRotation = Quaternion.Euler(0.0f, 0.0f, _zRotationOffset + Time.time * zRotationSpeed);
            body.localPosition =  new Vector3(0.0f, 0.0f, (Mathf.Sin(2.0f * Mathf.PI * _floatCycleOffset + (2.0f * Mathf.PI * Time.time) * floatFrequency) * 0.5f) * floatAmplitude - zShift);
        }

        private void FixedUpdate()
        {
            _rigidbody.rotation = Quaternion.LookRotation(Root.Instance.planet.transform.position - _rigidbody.position);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.collider != Root.Instance.planet.meshCollider)
                Physics.IgnoreCollision(_boxCollider, other.collider, true);
            else
                this.PlayAudioClip(thudAudioClip, transform.position, 1.0f, 1.0f);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == Root.Instance.playerGameObject)
            {
                this.PlayAudioClip(pickUpAudioClip, transform.position, 0.25f);
                Destroy(gameObject);
                Root.Instance.itemsCollected++;
                Root.Instance.itemCountOnPlanet--;
            }
        }
    }
}
