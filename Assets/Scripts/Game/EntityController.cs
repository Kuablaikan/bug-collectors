using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(Rigidbody))]
    public class EntityController : MonoBehaviour
    {
        public Transform target;
        public Vector2 heading = Vector2.up;
        public float headingForce = 10.0f;
        
        private Rigidbody _rigidbody;
        
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            if (_rigidbody == null)
                throw new MissingComponentException("Rigidbody is needed for EntityController");
        }
        
        private void FixedUpdate()
        {
            if (target == null)
                return;
            if (heading.magnitude <= 0.0f)
                return;
            
            Vector3 origin = Root.Instance.planet.transform.position;
            Vector3 position = _rigidbody.position;
            Vector3 normal = (origin - position).normalized; // it's not necessary to normalize
            Vector3 relativeTarget = (target.position - position).normalized; // it's not necessary to normalize here either
            Vector3 direction = (relativeTarget - Vector3.Dot(relativeTarget, normal) * normal).normalized;
            
            float fi = Mathf.Atan2(-heading.x, heading.y);
            Vector3 force = (Quaternion.AngleAxis(fi * Mathf.Rad2Deg, normal) * direction).normalized;
            
            _rigidbody.AddForce(force * headingForce);
        }
    }
}
