﻿using UnityEngine;

namespace Game
{
    public static class QuaternionExtensions
    {
        public static Quaternion ClampAngle(this Quaternion quaternion, float maxAngle)
        {
            quaternion.ToAngleAxis(out float angle, out Vector3 axis);
            return Quaternion.AngleAxis(Mathf.Clamp(angle, -maxAngle, maxAngle), axis);
        }
    }
}