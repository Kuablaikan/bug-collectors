using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(Rigidbody))]
    public class Gravity : MonoBehaviour
    {
        public float force = 10.0f;
        
        private Rigidbody _rigidbody;
        
        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            if (_rigidbody == null)
                throw new MissingComponentException("Rigidbody is needed for Gravity");
        }

        private void FixedUpdate()
        {
            _rigidbody.AddForce((Root.Instance.planet.transform.position - _rigidbody.position).normalized * force);
        }
    }
}
