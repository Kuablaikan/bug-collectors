﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class IcosahedronBodyGenerator
    {
        private readonly struct TriangleIndices
        {
            public readonly int v1;
            public readonly int v2;
            public readonly int v3;

            public TriangleIndices(int v1, int v2, int v3)
            {
                this.v1 = v1;
                this.v2 = v2;
                this.v3 = v3;
            }
        }
        
        public static GameObject CreateBodies(Material material, Vector3 velocity)
        {
            List<Vector3> vertices = new List<Vector3>(12);

            void AddVertex(Vector3 p)
            {
                float recHalfLength = 0.5f / Mathf.Sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
                vertices.Add(new Vector3(p.x * recHalfLength, p.y * recHalfLength, p.z * recHalfLength));
            }

            // create 12 vertices of a icosahedron
            float t = (1.0f + Mathf.Sqrt(5.0f)) * 0.5f;

            AddVertex(new Vector3(-1,  t,  0));
            AddVertex(new Vector3( 1,  t,  0));
            AddVertex(new Vector3(-1, -t,  0));
            AddVertex(new Vector3( 1, -t,  0));

            AddVertex(new Vector3( 0, -1,  t));
            AddVertex(new Vector3( 0,  1,  t));
            AddVertex(new Vector3( 0, -1, -t));
            AddVertex(new Vector3( 0,  1, -t));

            AddVertex(new Vector3( t,  0, -1));
            AddVertex(new Vector3( t,  0,  1));
            AddVertex(new Vector3(-t,  0, -1));
            AddVertex(new Vector3(-t,  0,  1));
            
            // create 20 triangles of the icosahedron
            var faces = new List<TriangleIndices>();

            // 5 faces around point 0
            faces.Add(new TriangleIndices(0, 11, 5));
            faces.Add(new TriangleIndices(0, 5, 1));
            faces.Add(new TriangleIndices(0, 1, 7));
            faces.Add(new TriangleIndices(0, 7, 10));
            faces.Add(new TriangleIndices(0, 10, 11));

            // 5 adjacent faces 
            faces.Add(new TriangleIndices(1, 5, 9));
            faces.Add(new TriangleIndices(5, 11, 4));
            faces.Add(new TriangleIndices(11, 10, 2));
            faces.Add(new TriangleIndices(10, 7, 6));
            faces.Add(new TriangleIndices(7, 1, 8));

            // 5 faces around point 3
            faces.Add(new TriangleIndices(3, 9, 4));
            faces.Add(new TriangleIndices(3, 4, 2));
            faces.Add(new TriangleIndices(3, 2, 6));
            faces.Add(new TriangleIndices(3, 6, 8));
            faces.Add(new TriangleIndices(3, 8, 9));

            // 5 adjacent faces 
            faces.Add(new TriangleIndices(4, 9, 5));
            faces.Add(new TriangleIndices(2, 4, 11));
            faces.Add(new TriangleIndices(6, 2, 10));
            faces.Add(new TriangleIndices(8, 6, 7));
            faces.Add(new TriangleIndices(9, 8, 1));

            GameObject parent = new GameObject("Icosahedron");
            List<GameObject> bodies = new List<GameObject>(20);
            int index = 1;
            foreach (var tri in faces)
            {
                GameObject body = new GameObject("Tetrahedron " + index);
                body.transform.parent = parent.transform;
                body.layer = LayerMask.NameToLayer("Debris");
                MeshFilter meshFilter = body.AddComponent<MeshFilter>();
                MeshRenderer meshRenderer = body.AddComponent<MeshRenderer>();
                meshRenderer.material = material;
                
                Mesh mesh = new Mesh();
                mesh.vertices = new Vector3[4] { vertices[tri.v1], vertices[tri.v2], vertices[tri.v3], new Vector3(0.0f, 0.0f, 0.0f) };
                mesh.triangles = new int[12] { 0, 1, 2, 3, 1, 0, 3, 0, 2, 3, 2, 1 };
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();
                mesh.RecalculateTangents();
                mesh.Optimize();
                meshFilter.mesh = mesh;

                MeshCollider meshCollider = body.AddComponent<MeshCollider>();
                meshCollider.sharedMesh = mesh;
                meshCollider.convex = true;
                Physics.IgnoreCollision(meshCollider, Root.Instance.planet.sphereCollider);
                Rigidbody rigidbody = body.AddComponent<Rigidbody>();
                rigidbody.useGravity = false;
                rigidbody.drag = 1.0f;
                rigidbody.angularDrag = 1.0f;
                if (UnityEngine.Random.value < 0.85f)
                    rigidbody.velocity = velocity;
                else
                    rigidbody.velocity = (rigidbody.centerOfMass - parent.transform.position).normalized * 20.0f;
                //rigidbody.velocity = (rigidbody.centerOfMass - parent.transform.position).normalized * 20.0f;
                body.AddComponent<Gravity>();
            }

            return parent;
        }
    }
}
