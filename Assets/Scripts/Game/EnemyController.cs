using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshCollider))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(EntityController))]
    public class EnemyController : MonoBehaviour
    {
        public MeshCollider triggerMeshCollider;
        public AudioClip thudAudioClip;
        public AudioClip popAudioClip;
        
        private MeshCollider _meshCollider;
        private Rigidbody _rigidbody;
        private EntityController _entityController;
        private bool _hasLanded = false;
        
        private void Awake()
        {
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            meshFilter.mesh = IcosphereGenerator.CreateMesh(0);
            MeshCollider meshCollider = GetComponent<MeshCollider>();
            meshCollider.sharedMesh = meshFilter.mesh;            
            _meshCollider = GetComponent<MeshCollider>();
            if (_meshCollider == null)
                throw new MissingComponentException("MeshCollider is needed for EnemyController");
            _meshCollider.sharedMesh = meshFilter.mesh;
            if (triggerMeshCollider == null)
                throw new InvalidOperationException("EnemyController.triggerMeshCollider is not set");
            triggerMeshCollider.sharedMesh = meshFilter.mesh;
            _rigidbody = GetComponent<Rigidbody>();
            if (_rigidbody == null)
                throw new MissingComponentException("Rigidbody is needed for EnemyController");
            _entityController = GetComponent<EntityController>();
            if (_entityController == null)
                throw new MissingComponentException("EntityController is needed for EnemyController");
            if (thudAudioClip == null)
                throw new InvalidOperationException("EnemyController.thudAudioClip is not set");
            if (popAudioClip == null)
                throw new InvalidOperationException("EnemyController.popAudioClip is not set");
        }

        private void Start()
        {
            _entityController.target = Root.Instance.playerGameObject.transform;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.collider == Root.Instance.planet.meshCollider)
            {
                if (!_hasLanded)
                {
                    _hasLanded = true;
                    _entityController.heading = new Vector2(0.0f, 1.0f);
                }
                if (Vector3.Project(_rigidbody.velocity, Root.Instance.planet.transform.position - _rigidbody.position).magnitude > 2.0f)
                    this.PlayAudioClip(thudAudioClip, transform.position, 1.0f, 1.0f);
            }
            else if (other.collider == Root.Instance.planet.sphereCollider)
                Physics.IgnoreCollision(_meshCollider, other.collider, true);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))
            {
                this.PlayAudioClip(popAudioClip, transform.position, 1.0f, 1.0f);
                GameObject obj = IcosahedronBodyGenerator.CreateBodies(GetComponent<MeshRenderer>().material, _rigidbody.velocity);
                obj.transform.position = transform.position;
                obj.transform.rotation = transform.rotation;
                Destroy(obj, 5.0f);
                Destroy(gameObject);
                Root.Instance.enemyCountOnPlanet--;
                Root.Instance.enemiesKilled++;
            }
        }
    }
}
