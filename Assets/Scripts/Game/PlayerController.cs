using System;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(EntityController))]
    public class PlayerController : MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private EntityController _entityController;
        private Transform _controllerTarget;
        
        public CameraController followingCamera;
        public Transform guns;
        public Transform leftGun;
        public Transform rightGun;
        public AudioClip laserAudioClip;
        
        public float reshootTime = 0.1f;
        private bool gunSwitch;
        private float lastShootTime = 0.0f;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            if (_rigidbody == null)
                throw new MissingComponentException("Rigidbody is needed for PlayerController");
            _entityController = GetComponent<EntityController>();
            if (_entityController == null)
                throw new MissingComponentException("EntityController is needed for PlayerController");
            _controllerTarget = new GameObject("Player Controller Target").transform;
            _entityController.target = _controllerTarget;
            if (followingCamera == null)
                throw new InvalidOperationException("PlayerController.followingCamera is not set");
            if (guns == null)
                throw new InvalidOperationException("PlayerController.guns is not set");
            if (leftGun == null)
                throw new InvalidOperationException("PlayerController.leftGun is not set");
            if (rightGun == null)
                throw new InvalidOperationException("PlayerController.rightGun is not set");
            if (laserAudioClip == null)
                throw new InvalidOperationException("PlayerController.laserAudioClip is not set");
        }
        
        private void Update()
        {
            _entityController.heading.x = Input.GetAxis("Horizontal");
            _entityController.heading.y = Input.GetAxis("Vertical");
            _controllerTarget.position = transform.position + followingCamera.transform.rotation * Vector3.forward;
            
            guns.position = transform.position;
            guns.rotation = Quaternion.FromToRotation(followingCamera.transform.rotation * Vector3.forward, (followingCamera.aimPosition - guns.position).normalized) * followingCamera.transform.rotation;
            leftGun.rotation = Quaternion.FromToRotation(guns.rotation * Vector3.forward, (followingCamera.aimPosition - leftGun.position).normalized).ClampAngle(15.0f) * guns.rotation;
            rightGun.rotation = Quaternion.FromToRotation(guns.rotation * Vector3.forward, (followingCamera.aimPosition - rightGun.position).normalized).ClampAngle(15.0f) * guns.rotation;

            if (Input.GetAxis("Fire1") > 0.5f && Time.time - lastShootTime >= reshootTime)
            {
                lastShootTime = Time.time;
                Vector3 position;
                Quaternion rotation;
                if (gunSwitch)
                {
                    position = leftGun.position;
                    rotation = leftGun.rotation;
                }
                else
                {
                    position = rightGun.position;
                    rotation = rightGun.rotation;
                }
                GameObject bullet = this.Instantiate(Root.Instance.bulletPrefab, position, rotation, "Bullet");
                bullet.GetComponent<Rigidbody>().velocity = rotation * Vector3.forward * 50.0f + _rigidbody.velocity;
                Destroy(bullet, 2.0f);
                gunSwitch = !gunSwitch;
                this.PlayAudioClip(laserAudioClip, position, 0.25f);
            }
        }
    }
}
