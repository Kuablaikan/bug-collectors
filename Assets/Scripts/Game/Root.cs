using System;
using System.Collections;
using UnityEngine;

namespace Game
{
    public class Root : MonoBehaviour
    {
        public static Root Instance { get; private set; }
        
        public GameObject playerPrefab;
        public GameObject itemPrefab;
        public GameObject bulletPrefab;
        public GameObject enemyPrefab;
        
        public Planet planet;
        public GameObject playerGameObject;

        public float itemSpawnDistance = 100.0f;
        public int maxItemCountOnPlanet = 100;
        public int itemCountOnPlanet = 0;
        public int itemsCollected = 0;
        public float itemSpwnInterval = 0.25f;
        
        public float enemySpawnDistance = 100.0f;
        public int maxEnemyCountOnPlanet = 100;
        public int enemyCountOnPlanet = 0;
        public int enemiesKilled = 0;
        public float enemySpwnInterval = 1.0f;

        public TMPro.TextMeshProUGUI text;
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            if (playerPrefab == null)
                throw new InvalidOperationException("Root.playerPrefab is not set");
            if (itemPrefab == null)
                throw new InvalidOperationException("Root.itemPrefab is not set");
            if (bulletPrefab == null)
                throw new InvalidOperationException("Root.bulletPrefab is not set");
            if (enemyPrefab == null)
                throw new InvalidOperationException("Root.enemyPrefab is not set");
            if (planet == null)
                throw new InvalidOperationException("Root.planet is not set");
        }

        private void Start()
        {
            StartCoroutine(SpawnItems());
            StartCoroutine(SpawnEnemies());
        }

        private void Update()
        {
            text.text = "Items collected: " + itemsCollected + "\r\nEnemies killed: " + enemiesKilled;
        }

        IEnumerator SpawnItems()
        {
            while (true)
            {
                for (; itemCountOnPlanet < maxItemCountOnPlanet; itemCountOnPlanet++)
                {
                    yield return new WaitForSeconds(itemSpwnInterval);
                    this.Instantiate(itemPrefab, UnityEngine.Random.onUnitSphere * itemSpawnDistance, Quaternion.identity, "Item");
                }
                yield return new WaitForSeconds(itemSpwnInterval);
            }
        }
        
        IEnumerator SpawnEnemies()
        {
            while (true)
            {
                for (; enemyCountOnPlanet < maxEnemyCountOnPlanet; enemyCountOnPlanet++)
                {
                    yield return new WaitForSeconds(enemySpwnInterval);
                    this.Instantiate(enemyPrefab, UnityEngine.Random.onUnitSphere * enemySpawnDistance, Quaternion.identity, "Enemy");
                }
                yield return new WaitForSeconds(enemySpwnInterval);
            }
        }
    }
}
