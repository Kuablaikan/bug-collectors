using UnityEngine;

namespace Game
{
    public static class UnityEngineObjectExtensions
    {
        public static GameObject Instantiate(this UnityEngine.Object obj, GameObject original, string name)
        {
            GameObject gameObject = Object.Instantiate(original);
            gameObject.name = name;
            return gameObject;
        }
        
        public static GameObject Instantiate(this UnityEngine.Object obj, GameObject original, Vector3 position, Quaternion rotation, string name)
        {
            GameObject gameObject = Object.Instantiate(original, position, rotation);
            gameObject.name = name;
            return gameObject;
        }
        
        public static void PlayAudioClip(this UnityEngine.Object obj, AudioClip clip, Vector3 position, float volume = 1.0f, float spatialBlend = 0.0f)
        {
            GameObject gameObject = new GameObject("One shot audio");
            gameObject.transform.position = position;
            AudioSource audioSource = (AudioSource)(gameObject.AddComponent(typeof (AudioSource)));
            audioSource.clip = clip;
            audioSource.spatialBlend = spatialBlend;
            audioSource.volume = volume;
            audioSource.Play();
            Object.Destroy((UnityEngine.Object)(gameObject), clip.length * ((double)(Time.timeScale) < 0.00999999977648258 ? 0.01f : Time.timeScale));
        }
    }
}
