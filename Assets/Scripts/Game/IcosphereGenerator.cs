using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public static class IcosphereGenerator
    {
        private readonly struct TriangleIndices
        {
            public readonly int v1;
            public readonly int v2;
            public readonly int v3;

            public TriangleIndices(int v1, int v2, int v3)
            {
                this.v1 = v1;
                this.v2 = v2;
                this.v3 = v3;
            }
        }
        
        public static Mesh CreateMesh(int recursionLevel)
        {
            static int GetVertexCount(int recursionLevel)
            {
                int count = 12;
                int triangles = 20;
                for (int level = 1; level <= recursionLevel; level++)
                {
                    count += triangles * 3;
                    triangles *= 4;
                }
                return count;
            }
            
            List<Vector3> vertices = new List<Vector3>(GetVertexCount(recursionLevel));
            Dictionary<long, int> middlePointIndexCache = new Dictionary<long, int>();
            int index = 0;
            
            int AddVertex(Vector3 p)
            {
                float recHalfLength = 0.5f / Mathf.Sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
                vertices.Add(new Vector3(p.x * recHalfLength, p.y * recHalfLength, p.z * recHalfLength));
                return index++;
            }
            
            int GetMiddlePoint(int p1, int p2)
            {
                // first check if we have it already
                bool firstIsSmaller = p1 < p2;
                long smallerIndex = firstIsSmaller ? p1 : p2;
                long greaterIndex = firstIsSmaller ? p2 : p1;
                long key = (smallerIndex << 32) + greaterIndex;

                int ret;
                if (middlePointIndexCache.TryGetValue(key, out ret))
                    return ret;

                // not in cache, calculate it
                Vector3 point1 = vertices[p1];
                Vector3 point2 = vertices[p2];
                Vector3 middle = new Vector3(
                    (point1.x + point2.x) * 0.5f, 
                    (point1.y + point2.y) * 0.5f, 
                    (point1.z + point2.z) * 0.5f);

                // add vertex makes sure point is on unit sphere
                int i = AddVertex(middle); 

                // store it, return index
                middlePointIndexCache.Add(key, i);
                return i;
            }

            // create 12 vertices of a icosahedron
            float t = (1.0f + Mathf.Sqrt(5.0f)) * 0.5f;

            AddVertex(new Vector3(-1,  t,  0));
            AddVertex(new Vector3( 1,  t,  0));
            AddVertex(new Vector3(-1, -t,  0));
            AddVertex(new Vector3( 1, -t,  0));

            AddVertex(new Vector3( 0, -1,  t));
            AddVertex(new Vector3( 0,  1,  t));
            AddVertex(new Vector3( 0, -1, -t));
            AddVertex(new Vector3( 0,  1, -t));

            AddVertex(new Vector3( t,  0, -1));
            AddVertex(new Vector3( t,  0,  1));
            AddVertex(new Vector3(-t,  0, -1));
            AddVertex(new Vector3(-t,  0,  1));
            
            // create 20 triangles of the icosahedron
            var faces = new List<TriangleIndices>();

            // 5 faces around point 0
            faces.Add(new TriangleIndices(0, 11, 5));
            faces.Add(new TriangleIndices(0, 5, 1));
            faces.Add(new TriangleIndices(0, 1, 7));
            faces.Add(new TriangleIndices(0, 7, 10));
            faces.Add(new TriangleIndices(0, 10, 11));

            // 5 adjacent faces 
            faces.Add(new TriangleIndices(1, 5, 9));
            faces.Add(new TriangleIndices(5, 11, 4));
            faces.Add(new TriangleIndices(11, 10, 2));
            faces.Add(new TriangleIndices(10, 7, 6));
            faces.Add(new TriangleIndices(7, 1, 8));

            // 5 faces around point 3
            faces.Add(new TriangleIndices(3, 9, 4));
            faces.Add(new TriangleIndices(3, 4, 2));
            faces.Add(new TriangleIndices(3, 2, 6));
            faces.Add(new TriangleIndices(3, 6, 8));
            faces.Add(new TriangleIndices(3, 8, 9));

            // 5 adjacent faces 
            faces.Add(new TriangleIndices(4, 9, 5));
            faces.Add(new TriangleIndices(2, 4, 11));
            faces.Add(new TriangleIndices(6, 2, 10));
            faces.Add(new TriangleIndices(8, 6, 7));
            faces.Add(new TriangleIndices(9, 8, 1));
            
            // refine triangles
            for (int i = 0; i < recursionLevel; i++)
            {
                var auxFaces = new List<TriangleIndices>(faces.Count * 4);
                foreach (var tri in faces)
                {
                    // replace triangle by 4 triangles
                    int a = GetMiddlePoint(tri.v1, tri.v2);
                    int b = GetMiddlePoint(tri.v2, tri.v3);
                    int c = GetMiddlePoint(tri.v3, tri.v1);
                    auxFaces.Add(new TriangleIndices(tri.v1, a, c));
                    auxFaces.Add(new TriangleIndices(tri.v2, b, a));
                    auxFaces.Add(new TriangleIndices(tri.v3, c, b));
                    auxFaces.Add(new TriangleIndices(a, b, c));
                }
                faces = auxFaces;
            }

            List<int> triangles = new List<int>(faces.Count * 3);
            foreach (var tri in faces)
            {
                triangles.Add(tri.v1);
                triangles.Add(tri.v2);
                triangles.Add(tri.v3);
            }

            Mesh mesh = new Mesh();
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.Optimize();
            return mesh;
        }
    }
}
