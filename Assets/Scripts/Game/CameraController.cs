﻿using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        public Transform target;
        public float maxRotationDelta = 360.0f;
        public float rotationSensitivity = 10.0f;
        public float minYAngle = 5.0f;
        public float maxYAngle = 100.0f;
        public float distance = 10.0f;
        public float smoothness = 20.0f;
        public Vector3 offset;
        public float colliderSize = 0.05f;
        public Vector3 aimPosition;
        public float aimFocusDistance = 25.0f;
        
        private Transform _target;
        private Vector2 currentRotation;
        private Vector2 rotation;

        private void Awake()
        {
            _target = new GameObject("Camera Controller Target").transform;
        }
        
        private void Update()
        {
            static float Cosp(float a, float b, float t)
            {
                float aux = (1.0f - Mathf.Cos(t * Mathf.PI)) * 0.5f;
                return (a * (1.0f - aux) + b * aux);
            }
            
            if (target == null)
                return;

            rotation.x += Input.GetAxis("Mouse X") * rotationSensitivity;
            rotation.y += Input.GetAxis("Mouse Y") * rotationSensitivity;
            rotation.y = Mathf.Clamp(rotation.y, minYAngle, maxYAngle);
            currentRotation = new Vector2(
                Cosp(currentRotation.x, rotation.x, Time.deltaTime * smoothness),
                Cosp(currentRotation.y, rotation.y, Time.deltaTime * smoothness));
            
            Quaternion targetRotation = Quaternion.FromToRotation(_target.rotation * Vector3.forward, (Root.Instance.planet.transform.position - target.position).normalized) * _target.rotation;
            _target.rotation = Quaternion.RotateTowards(_target.rotation, targetRotation,  Time.deltaTime * maxRotationDelta);
            _target.position = target.position;
            
            Quaternion quaternion = Quaternion.Euler(0.0f, 0.0f, -currentRotation.x) * Quaternion.Euler(currentRotation.y, 0.0f, 180.0f);
            quaternion.ToAngleAxis(out float angle, out Vector3 axis);
            Vector3 realOffset = new Vector3(0.0f, 0.0f, -distance) + offset;
            transform.position = _target.position + _target.TransformDirection(quaternion * realOffset);
            transform.rotation = _target.rotation * Quaternion.AngleAxis(-angle, -axis);

            float dist = realOffset.magnitude;
            RaycastHit hit;
            if (Physics.Raycast(transform.position + transform.rotation * Vector3.forward * dist, transform.rotation * Vector3.back, out hit, dist, LayerMask.GetMask("Planet")))
            {
                transform.position = hit.point + transform.rotation * Vector3.forward * colliderSize;
                dist = hit.distance - colliderSize;
            }

            aimPosition = transform.position + transform.rotation * Vector3.forward * aimFocusDistance;
            if (Physics.Raycast(transform.position + transform.rotation * Vector3.forward * dist, transform.rotation * Vector3.forward, out hit, 1000.0f, LayerMask.GetMask("Default", "Planet", "Enemy")))
                aimPosition = hit.point;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawSphere(aimPosition, 0.1f);
        }
    }
}
