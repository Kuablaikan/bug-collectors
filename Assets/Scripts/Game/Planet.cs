using System;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(MeshFilter))]
    public class Planet : MonoBehaviour
    {
        [Range(0, 8)] 
        public int icosphereDetailsLevel;

        public MeshCollider meshCollider;
        public SphereCollider sphereCollider;
        
        private void Awake()
        {
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            if (meshFilter == null)
                throw new MissingComponentException("MeshFilter is needed for Planet");
            if (meshCollider == null)
                throw new InvalidOperationException("Planet.meshCollider is not set");
            if (sphereCollider == null)
                throw new InvalidOperationException("Planet.sphereCollider is not set");
            meshFilter.mesh = IcosphereGenerator.CreateMesh(icosphereDetailsLevel);
            meshCollider.sharedMesh = meshFilter.mesh;
        }
    }
}
